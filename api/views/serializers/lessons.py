from api.views.serializers.fields import LessonDataField
from api.models import Lesson
from rest_framework import serializers


class LessonDataSerializer(serializers.Serializer):
    data = LessonDataField(required=False)


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = (
            'id', 'owner', 'created', 'modified', 'data', 'tags'
        )

        read_only_fields = (
            'id', 'owner', 'created', 'modified', 'data', 'tags'
        )
