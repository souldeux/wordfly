from django.test import TestCase
from rest_framework.test import APIClient
from api.models import User


class UserAccountEndpointTestCase(TestCase):
    def testAnonAccount(self):
        resp = APIClient().get('/api/v1/users/account/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()['status'], 'ok')
        self.assertIsNone(resp.json()['email'])
        self.assertFalse(resp.json()['staff'])

    def testAuthAccount(self):
        user = User.objects.create_user(
            email='test@email.com',
            password='test'
        )
        client = APIClient()
        client.credentials(
            HTTP_AUTHORIZATION='Token ' + user.spa_token)

        resp = client.get('/api/v1/users/account/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()['status'], 'ok')
        self.assertEqual(resp.json()['email'], user.email)
        self.assertFalse(resp.json()['staff'])

        user.is_staff = True
        user.save()
        resp = client.get('/api/v1/users/account/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.json()['status'], 'ok')
        self.assertEqual(resp.json()['email'], user.email)
        self.assertTrue(resp.json()['staff'])
