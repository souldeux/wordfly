from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class UserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The Email must be set.")
        email = self.normalize_email(email).lower()
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user


class User(AbstractUser):
    created = models.DateTimeField(
        db_index=True,
        auto_now_add=True
    )
    email = models.EmailField(
        unique=True,
        db_index=True
    )

    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    date_joined = None

    objects = UserManager()

    @property
    def spa_token(self):
        if Token.objects.filter(user=self).exists():
            return Token.objects.filter(user=self).first().key
        return Token.objects.get_or_create(user=self)[0].key
