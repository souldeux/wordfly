from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from api.models import Wordlist
from .serializers import WordlistSerializer


class WordlistViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    '''
    Viewset to provide unauthenticated read-only list & detail view access to Wordlists.
    '''
    model = Wordlist
    queryset = model.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = WordlistSerializer
