from django.db import models


class BaseModel(models.Model):
    created = models.DateTimeField(db_index=True, auto_now_add=True)
    modified = models.DateTimeField(db_index=True, auto_now=True)
    owner = models.ForeignKey('User', null=False, on_delete=models.CASCADE)

    def refresh_from_db(self, *args, **kwargs):
        super().refresh_from_db(*args, **kwargs)
        for field in self._meta.concrete_fields:
            if field.is_relation and field.get_cache_name() in self.__dict__:
                del self.__dict__[field.get_cache_name()]

        try:
            del self.__dict__['_prefetched_objects_cache']
        except KeyError:
            pass

    class Meta:
        abstract = True
