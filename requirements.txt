Django==3.*
gunicorn
greenlet
gevent
djangorestframework
django-extensions
coverage
jsonschema
pyyaml
whitenoise
tblib
django-cors-headers
psycogreen

django-elasticsearch-dsl
django-elasticsearch-dsl-drf
elasticsearch-dsl
