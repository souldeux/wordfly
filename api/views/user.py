from rest_framework import viewsets, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from api.models import User
from .serializers import AuthSerializer


class UserViewSet(viewsets.GenericViewSet):
    '''
    Vieset to provide authentication for registered users
    '''

    MSG_INVALID_LOGIN = 'Invalid login credentials'
    permission_classes = (AllowAny,)

    @action(detail=False, methods=['post'])
    def auth(self, request, *args, **kwargs):
        serializer = AuthSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            user = User.objects.get(
                email=serializer.data['email'].strip().lower()
            )
        except User.DoesNotExist:
            return Response(
                {'error': self.MSG_INVALID_LOGIN},
                status=400
            )

        if not user.check_password(serializer.data['password']):
            return Response(
                {'error': self.MSG_INVALID_LOGIN},
                status=400
            )

        return Response(
            {'status': 'ok', 'token': user.spa_token},
            status=200
        )

    @action(detail=False, methods=['get'])
    def account(self, request, *args, **kwargs):
        return Response(
            {
                'status': 'ok',
                'email': request.user.email if request.user.is_authenticated else None,
                'staff': request.user.is_staff if request.user.is_authenticated else False
            },
            status=200
        )
